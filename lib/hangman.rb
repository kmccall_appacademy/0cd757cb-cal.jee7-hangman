class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
  end

  def update_board(guess, matches)
    matches.each do |idx|
      @board[idx] = guess
    end
  end

  def setup
    length = @referee.pick_secret_word
    @guesser.register_secret_length(length)
    @board = Array.new(length)
  end

  def play
    setup
    until won?
      print "Secret word: #{print_board}"
      take_turn
    end
    puts "Congratulations! The secret word was #{print_board}."
  end

  def print_board
    @board.reduce("") do |word, letter|
      if letter.nil?
        word += "_"
      else
        word += letter
      end
      word
    end
  end

  def take_turn
    guess = @guesser.guess(@board)
    matches = @referee.check_guess(guess)
    update_board(guess, matches)
  end

  def won?
    !@board.include?(nil)
  end

end

class HumanPlayer
  attr_reader :dictionary, :secret_length

  def initialize(dictionary = nil)
    @dictionary = dictionary
    @word = nil
  end

  def pick_secret_word
    puts "What is your secret word?"
    @word = $stdin.gets.chomp.downcase
    @word.length
  end

  def register_secret_length(length)
    @secret_length = length
  end

  def guess(board)
    letters_available = ("a".."z").to_a
    guessed_letter = nil
    until letters_available.include?(guessed_letter)
      guessed_letter = gets.chomp.downcase
    end
    guessed_letter
  end

  def check_guess(guess, word = @word)
    correct_indices = get_indices(guess, word)
    handle_response(guess, correct_indices)
    correct_indices
  end

  def handle_response(guess, matches)
    if matches.empty?
      puts "Sorry! The letter '#{guess}' is not in the word."
    else
      puts "Yes! You've guessed a correct letter."
    end
  end

  def update_board(guess, matches)
    matches.each do |idx|
      @board[idx] = guess
    end
  end

  private

  def get_indices(letter, word)
    correct_indices = []
    word.chars.each_with_index do |char, idx|
      correct_indices << idx if char == letter
    end
    correct_indices
  end

end

class ComputerPlayer
  attr_reader :dictionary, :secret_length
  attr_accessor :candidate_words, :candidate_letters

  # WORDS = File.readlines("dictionary.txt").map!{|word| word.delete("\n")}

  def initialize(dictionary = WORDS)
    @dictionary = dictionary
    @word = nil
    @candidate_words = dictionary
  end

  def pick_secret_word
    @word = @dictionary[rand(@dictionary.length)].downcase
    @word.length
  end

  def register_secret_length(length)
    @candidate_words.select! { |word| word.length == length }
    @secret_length = length
  end

  def guess(board)
    smart_letters = candidate_letters(board)
    smart_letters.max_by{|k,v| v}[0]
  end

  def check_guess(guess, word = @word)
    correct_indices = get_indices(guess, word)
    handle_response(guess, correct_indices)
    correct_indices
  end

  def handle_response(guess, matches)
    if matches.empty?
      remove_words_without_letter(guess)
      puts "Sorry! The letter '#{guess}' is not in the word."
    else
      remove_words_without_matches(guess, matches)
      puts "Yes! You've guessed a correct letter."
    end
  end

  def update_board(guess, matches)
    matches.each do |idx|
      @board[idx] = guess
    end
  end

  def candidate_letters(board)
    letter_count = Hash.new(0)
    @candidate_words.each do |word|
      word.chars.each do |char|
        letter_count[char] += 1 if !board.include?(char)
      end
    end
    letter_count
  end

  private

  def remove_words_without_letter(letter)
    @candidate_words.each do |word|
      @candidate_words.delete(word) if word.include?(letter)
    end
  end

  def remove_words_without_matches(letter, matches)
    @candidate_words =
      @candidate_words.reduce([]) do |upd_words, word|
        word_indices = get_indices(letter, word)
        if word_indices == matches
          upd_words << word
        end
        upd_words
      end
  end

  def get_indices(letter, word)
    correct_indices = []
    word.chars.each_with_index do |char, idx|
      correct_indices << idx if char == letter
    end
    correct_indices
  end

end
